#include <iostream>
// #include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>

void drawText(cv::Mat& image)
{
    cv::putText(image, "Hello OpenCV",
                cv::Point(20, 50),
                cv::FONT_HERSHEY_COMPLEX, 1,
                cv::Scalar(255, 255, 255), 1,
                cv::LINE_AA);
}

int main()
{
    std::cout << "Built with OpenCV " << CV_VERSION << "\n";

    cv::Mat image {};
    cv::VideoCapture capture {};
    capture.open(0);
    if (capture.isOpened()) {
        std::cout << "Capture is opened.\n";

        for (;;) {
            capture >> image;
            if (image.empty())
                break;

            drawText(image);
            cv::imshow("Sample", image);

            if (cv::waitKey(10) >= 0)
                break;
        }
    } else {
        std::cerr << "Capture was not OPEN.\n";
        image = cv::Mat::zeros(480, 640, CV_8UC1);
        drawText(image);
        cv::imshow("Sample", image);
        cv::waitKey(0);
    }

    return 0;
}
