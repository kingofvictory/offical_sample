#include <cstdio>
#include <opencv2/opencv.hpp>

int main(int argc, char **argv)
{
    if (argc != 2) {
        printf("usage: resize IMAGE_FILE.\n");
        return -1;
    }

    cv::Mat image {cv::imread(argv[1], 1)};

    cv::Mat resize_image {};

    // Show image size.
    std::cout << image.size() << "\n";
    std::cout << "Image Width: " << image.cols << " " << "Image Height: " << image.rows << "\n";

    // Resize
    cv::resize(image, resize_image, cv::Size(2560, 1600), cv::INTER_AREA);
    // cv::resize(image, resize_image, cv::Size(), 0.5, 0.5); // another to scale.

    cv::Rect crop_rect(300, 300, 500, 500);
    cv::Mat crop_image {image(crop_rect)};

    cv::imshow("Original", image);
    cv::imshow("Resize", resize_image);
    cv::imshow("Crop", crop_image);

    // Save Image
    // cv::imwrite("./output.png", crop_image);

    cv::waitKey(0);
    cv::destroyAllWindows();

    return 0;
}

/*
  cv::resize()

  INTER_AREA:
  INTER_AREA uses pixel area relation for resampling. This is best suited for reducing the size of an image (shrinking). When used
  for zooming into the image, it uses the INTER_NEAREST method.

  INTER_CUBIC:
  This uses bicubic interpolation for resizing the image. While resizing and interpolating new pixels, this method acts on the 4×4
  neighboring pixels of the image. It then takes the weights average of the 16 pixels to create the new interpolated pixel.

  INTER_LINEAR:
  This method is somewhat similar to the INTER_CUBIC interpolation. But unlike INTER_CUBIC, this uses 2×2 neighboring pixels to get
  the weighted average for the interpolated pixel.

  INTER_NEAREST:
  The INTER_NEAREST method uses the nearest neighbor concept for interpolation. This is one of the simplest methods, using only one
  neighboring pixel from the image for interpolation.
 */
