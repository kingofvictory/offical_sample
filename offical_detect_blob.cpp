#include <map>
#include <vector>
#include <iostream>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>


static void help(char** argv)
{
    std::cout << "\n This program demonstrates how to use BLOB to detect and filter region \n"
         << "Usage: \n"
         << argv[0]
         << " <image1(detect_blob.png as default)>\n"
         << "Press a key when image window is active to change descriptor\n";
}


static cv::String Legende(cv::SimpleBlobDetector::Params& pAct)
{
    cv::String s {};
    
    if (pAct.filterByArea) {
        cv::String inf {static_cast<const std::ostringstream&>(std::ostringstream()
                                                                << pAct.minArea).str()};
        cv::String sup {static_cast<const std::ostringstream&>(std::ostringstream()
                                                               << pAct.maxArea).str()};
        
        s = " Area range [" + inf + " to  " + sup + "]";
    }
    
    if (pAct.filterByCircularity) {
        cv::String inf {static_cast<const std::ostringstream&>(std::ostringstream()
                                                                << pAct.minCircularity).str()};
        cv::String sup {static_cast<const std::ostringstream&>(std::ostringstream()
                                                               << pAct.maxCircularity).str()};
        
        if (s.length() == 0) {
            s = " Circularity range [" + inf + " to  " + sup + "]";
        } else {
            s += " AND Circularity range [" + inf + " to  " + sup + "]";
        }
    }
    
    if (pAct.filterByColor) {
        cv::String inf {static_cast<const std::ostringstream&>(std::ostringstream()
                                                                << (int)pAct.blobColor).str()};
        
        if (s.length() == 0) {
            s = " Blob color " + inf;
        } else {
            s += " AND Blob color " + inf;
        }
    }
    
    if (pAct.filterByConvexity) {
        cv::String inf {static_cast<const std::ostringstream&>(std::ostringstream()
                                                               << pAct.minConvexity).str()};
        cv::String sup {static_cast<const std::ostringstream&>(std::ostringstream()
                                                                << pAct.maxConvexity).str()};
        
        if (s.length() == 0) {
            s = " Convexity range[" + inf + " to  " + sup + "]";
        } else {
            s += " AND  Convexity range[" + inf + " to  " + sup + "]";
        }
    }
    
    if (pAct.filterByInertia) {
        cv::String inf {static_cast<const std::ostringstream&>(std::ostringstream()
                                                               << pAct.minInertiaRatio).str()};
        cv::String sup {static_cast<const std::ostringstream&>(std::ostringstream()
                                                                << pAct.maxInertiaRatio).str()};
        
        if (s.length() == 0) {
            s = " Inertia ratio range [" + inf + " to  " + sup + "]";
        } else {
            s += " AND  Inertia ratio range [" + inf + " to  " + sup + "]";
        }
    }
    
    return s;
}

int main(int argc, char *argv[])
{
    cv::String fileName {};
    
    cv::CommandLineParser parser(argc, argv,
                                 "{@input |detect_blob.png| }{h help | | }");
    
    if (parser.has("h")) {
        help(argv);
        return 0;
    }
    
    fileName = parser.get<std::string>("@input");
    cv::Mat img = imread(cv::samples::findFile(fileName), cv::IMREAD_COLOR);
    if (img.empty()) {
        std::cout << "Image " << fileName << " is empty or cannot be found\n";
        return 1;
    }

    cv::SimpleBlobDetector::Params pDefaultBLOB {};
    
    // This is default parameters for SimpleBlobDetector
    pDefaultBLOB.thresholdStep       = 10;
    pDefaultBLOB.minThreshold        = 10;
    pDefaultBLOB.maxThreshold        = 220;
    pDefaultBLOB.minRepeatability    = 2;
    pDefaultBLOB.minDistBetweenBlobs = 10;
    pDefaultBLOB.filterByColor       = false;
    pDefaultBLOB.blobColor           = 0;
    pDefaultBLOB.filterByArea        = false;
    pDefaultBLOB.minArea             = 25;
    pDefaultBLOB.maxArea             = 5000;
    pDefaultBLOB.filterByCircularity = false;
    pDefaultBLOB.minCircularity      = 0.9f;
    pDefaultBLOB.maxCircularity      = static_cast<float>(1e37);
    pDefaultBLOB.filterByInertia     = false;
    pDefaultBLOB.minInertiaRatio     = 0.1f;
    pDefaultBLOB.maxInertiaRatio     = static_cast<float>(1e37);
    pDefaultBLOB.filterByConvexity   = false;
    pDefaultBLOB.minConvexity        = 0.95f;
    pDefaultBLOB.maxConvexity        = static_cast<float>(1e37);

    // Descriptor array for BLOB
    std::vector<cv::String> typeDesc {};

// Param array for BLOB
    std::vector<cv::SimpleBlobDetector::Params>            pBLOB {};
    std::vector<cv::SimpleBlobDetector::Params>::iterator itBLOB {};

    // Color palette
    std::vector<cv::Vec3b >  palette;
    for (int i = 0; i<65536; i++) {
        uchar c1 = static_cast<uchar>(rand());
        uchar c2 = static_cast<uchar>(rand());
        uchar c3 = static_cast<uchar>(rand());
        palette.push_back(cv::Vec3b(c1, c2, c3));
    }
    help(argv);

    // These descriptors are going to be detecting and computing BLOBS with 6 different params
    // Param for first BLOB detector we want all
    typeDesc.push_back("BLOB");    // see http://docs.opencv.org/4.x/d0/d7a/classcv_1_1SimpleBlobDetector.html
    pBLOB.push_back(pDefaultBLOB);
    pBLOB.back().filterByArea = true;
    pBLOB.back().minArea      = 1;
    pBLOB.back().maxArea      = static_cast<float>(img.rows*img.cols);

    // Param for second BLOB detector we want area between 500 and 2900 pixels
    typeDesc.push_back("BLOB");
    pBLOB.push_back(pDefaultBLOB);
    pBLOB.back().filterByArea = true;
    pBLOB.back().minArea      = 500;
    pBLOB.back().maxArea      = 2900;

    // Param for third BLOB detector we want only circular object
    typeDesc.push_back("BLOB");
    pBLOB.push_back(pDefaultBLOB);
    pBLOB.back().filterByCircularity = true;
    
    // Param for Fourth BLOB detector we want ratio inertia
    typeDesc.push_back("BLOB");
    pBLOB.push_back(pDefaultBLOB);
    pBLOB.back().filterByInertia = true;
    pBLOB.back().minInertiaRatio = 0;
    pBLOB.back().maxInertiaRatio = static_cast<float>(0.2);
    
    // Param for fifth BLOB detector we want ratio inertia
    typeDesc.push_back("BLOB");
    pBLOB.push_back(pDefaultBLOB);
    pBLOB.back().filterByConvexity = true;
    pBLOB.back().minConvexity      = 0.;
    pBLOB.back().maxConvexity      = static_cast<float>(0.9);
    
    // Param for six BLOB detector we want blob with gravity center color equal to 0
    typeDesc.push_back("BLOB");
    pBLOB.push_back(pDefaultBLOB);
    pBLOB.back().filterByColor = true;
    pBLOB.back().blobColor     = 0;

    itBLOB = pBLOB.begin();
    std::vector<double> desMethCmp {};
    cv::Ptr<cv::Feature2D>       b {};
    cv::String               label {};
    
    // Descriptor loop
    std::vector<cv::String>::iterator itDesc {};
    for (itDesc = typeDesc.begin(); itDesc != typeDesc.end(); ++itDesc) {
        std::vector<cv::KeyPoint> keyImg1;
        if (*itDesc == "BLOB") {
            b = cv::SimpleBlobDetector::create(*itBLOB);
            label = Legende(*itBLOB);
            ++itBLOB;
        }

        try {
            // We can detect keypoint with detect method
            std::vector<cv::KeyPoint>            keyImg {};
            std::vector<cv::Rect>                zone   {};
            std::vector<std::vector<cv::Point>>  region {};
            cv::Mat     desc {}, result {img.rows, img.cols, CV_8UC3};
            
            if (b.dynamicCast<cv::SimpleBlobDetector>().get()) {
                cv::Ptr<cv::SimpleBlobDetector> sbd {b.dynamicCast<cv::SimpleBlobDetector>()};
                sbd->detect(img, keyImg, cv::Mat());
                cv::drawKeypoints(img, keyImg, result);
                
                int i {0};
                for (std::vector<cv::KeyPoint>::iterator k = keyImg.begin();
                     k != keyImg.end(); ++k, ++i)
                    cv::circle(result, k->pt, (int)k->size, palette[i % 65536]);
            }
            
            cv::namedWindow(*itDesc + label, cv::WINDOW_AUTOSIZE);
            cv::imshow(*itDesc + label, result);
            cv::imshow("Original", img);
            cv::waitKey();
        } catch (const cv::Exception& e) {
            std::cout << "Feature : " << *itDesc << "\n";
            std::cout << e.msg << "\n";
        }
    }
    
    return 0;
}