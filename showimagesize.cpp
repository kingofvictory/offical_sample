#include <iostream>
#include <opencv2/opencv.hpp>

int main(int argc, char **argv)
{
    if (argc != 2) {
	std::cerr << "usage: showimgsize IMAGE\nPlease select an IMAGE.\n";
	return -1;
    }

    cv::Mat image {cv::imread(argv[1], 1)};
    std::cout << "Image Width : " << image.cols << "\n";
    std::cout << "Image Height: " << image.rows << "\n";

    return 0;
}

/*
  cv::Mat.size()        return Width and Height
  cv::Mat.size().width  return Image Width
  cv::Mat.size().height return Image Height

  cv::Mat.cols          return Image Width
  cv::Mat.rows          return Image Height
 */
