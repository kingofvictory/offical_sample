#include <iostream>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/utility.hpp>

int edgeThresh       {1};
int edgeThreshScharr {1};

cv::Mat image {}, gray {}, blurImage {}, edge1 {}, edge2 {}, cedge {};

const std::string& window_name1 {"Edge map : Canny default (Sobel gradient)"};
const std::string& window_name2 {"Edge map : Canny with custom gradient (Scharr)"};

// define a trackbar callback
static void onTrackbar(int, void*)
{
    cv::blur(gray, blurImage, cv::Size(3,3));

    // Run the edge detector on grayscale
    cv::Canny(blurImage, edge1, edgeThresh, edgeThresh*3, 3);
    cedge = cv::Scalar::all(0);

    image.copyTo(cedge, edge1);
    cv::imshow(window_name1, cedge);                                        

    /// Canny detector with scharr
    cv::Mat dx {}, dy {};
    cv::Scharr(blurImage,dx,CV_16S,1,0);
    cv::Scharr(blurImage,dy,CV_16S,0,1);
    cv::Canny(dx,dy, edge2, edgeThreshScharr, edgeThreshScharr*3);

    /// Using Canny's output as a mask, we display our result
    cedge = cv::Scalar::all(0);
    image.copyTo(cedge, edge2);
    cv::imshow(window_name2, cedge);
    cv::imwrite("./output.png", cedge);
}

static void help(const char** argv)
{
    std::cout << "\nThis sample demonstrates Canny edge detection\n" <<
        "Call:\n" << 
        " [image_name -- Default is fruits.jpg]\n\n" << argv[0];
}

const std::string& keys {"{help h||}{@image |fruits.jpg|input image name}"};

int main( int argc, const char** argv )
{
    help(argv);
    cv::CommandLineParser parser(argc, argv, keys);
    std::string filename = parser.get<std::string>(0);

    image = cv::imread(cv::samples::findFile(filename), cv::IMREAD_COLOR);
    if (image.empty()) {
        printf("Cannot read image file: %s\n", filename.c_str());
        help(argv);
        return -1;
    }
    
    cedge.create(image.size(), image.type());
    cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);

    // Create a window
    cv::namedWindow(window_name1, 1);
    cv::namedWindow(window_name2, 1);

    // create a toolbar
    cv::createTrackbar("Canny threshold default", window_name1,
                       &edgeThresh, 100, onTrackbar);
    cv::createTrackbar("Canny threshold Scharr", window_name2,
                       &edgeThreshScharr, 400, onTrackbar);

    // Show the image
    onTrackbar(0, 0);

    // Wait for a key stroke; the same function arranges events processing
    cv::waitKey(0);

    return 0;
}