#include <iostream>
// #include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/objdetect.hpp>

static void help(const char** argv)
{
    std::cout << "\nThis program demonstrates the use of cv::CascadeClassifier class to detect objects (Face + eyes). You can use Haar or LBP features.\n"
            "This classifier can recognize many kinds of rigid objects, once the appropriate classifier is trained.\n"
            "It's most known use is for faces.\n"
            "Usage:\n"
        <<  argv[0]
        <<  "   [--cascade=<cascade_path> this is the primary trained classifier such as frontal face]\n"
            "   [--nested-cascade[=nested_cascade_path this an optional secondary classifier such as eyes]]\n"
            "   [--scale=<image scale greater or equal to 1, try 1.3 for example>]\n"
            "   [--try-flip]\n"
            "   [filename|camera_index]\n\n"
            "example:\n"
        <<  argv[0]
              <<  " --cascade=\"/home/alonzo/downloads/opencv-4.6.0/data/haarcascades/haarcascade_frontalface_alt.xml\"--nested-cascade=\"/home/alonzo/downloads/opencv-4.6.0/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml\" --scale=1.3\n\n"
            "During execution:\n\tHit any key to quit.\n"
            "\tUsing OpenCV version " << CV_VERSION << "\n";
}

void detectAndDraw(cv::Mat& img, cv::CascadeClassifier& cascade,
                   cv::CascadeClassifier& nestedCascade,
                   double scale, bool tryflip);

std::string cascadeName       {};
std::string nestedCascadeName {};

int main(int argc, const char** argv)
{
    cv::VideoCapture capture {};
    cv::Mat frame {}, image  {};
    std::string inputName    {};
    bool tryflip             {};
    cv::CascadeClassifier cascade {}, nestedCascade {};
    double scale             {};

    cv::CommandLineParser parser(argc, argv,
        "{help h||}"
        "{cascade|/home/alonzo/downloads/opencv-4.6.0/data/haarcascades/haarcascade_frontalface_alt.xml|}"
        "{nested-cascade|/home/alonzo/downloads/opencv-4.6.0/data/haarcascades/haarcascade_eye_tree_eyeglasses.xml|}"
        "{scale|1|}{try-flip||}{@filename||}"
    );

    if (parser.has("help"))
    {
        help(argv);
        return 0;
    }

    cascadeName = parser.get<std::string>("cascade");
    nestedCascadeName = parser.get<std::string>("nested-cascade");
    scale = parser.get<double>("scale");

    if (scale < 1)
        scale = 1;

    tryflip = parser.has("try-flip");
    inputName = parser.get<std::string>("@filename");

    if (!parser.check())
    {
        parser.printErrors();
        return 0;
    }
    
    if (!nestedCascade.load(cv::samples::findFileOrKeep(nestedCascadeName)))
        std::cerr << "WARNING: Could not load classifier cascade for nested objects\n";

    if (!cascade.load(cv::samples::findFile(cascadeName))) {
        std::cerr << "ERROR: Could not load classifier cascade\n";
        help(argv);
        return -1;
    }

    if (inputName.empty() || (isdigit(inputName[0]) && inputName.size() == 1)) {
        int camera = inputName.empty() ? 0 : inputName[0] - '0';
        if (!capture.open(camera)) {
            std::cout << "Capture from camera #" <<  camera << " didn't work\n";
            return 1;
        }
    } else if (!inputName.empty()) {
        image = cv::imread(cv::samples::findFileOrKeep(inputName),
                           cv::IMREAD_COLOR);
        if (image.empty()) {
            if (!capture.open(cv::samples::findFileOrKeep(inputName))) {
                std::cout << "Could not read " << inputName << "\n";
                return 1;
            }
        }
    } else {
        image = cv::imread(cv::samples::findFile("lena.jpg"), cv::IMREAD_COLOR);

        if (image.empty()) {
            std::cout << "Couldn't read lena.jpg\n";
            return 1;
        }
    }

    if (capture.isOpened()) {
        std::cout << "Video capturing has been started ...\n";

        for (;;) {
            capture >> frame;
            if (frame.empty())
                break;

            cv::Mat frame1 {frame.clone()};
            detectAndDraw(frame1, cascade, nestedCascade, scale, tryflip);

            char c {static_cast<char>(cv::waitKey(10))};
            if (c == 27 || c == 'q' || c == 'Q')
                break;
        }
    } else {
        std::cout << "Detecting face(s) in " << inputName << "\n";

        if (!image.empty()) {
            detectAndDraw( image, cascade, nestedCascade, scale, tryflip );
            cv::waitKey(0);
        } else if (!inputName.empty()) {
            /* assume it is a text file containing the
            list of the image filenames to be processed - one per line */
            FILE* f = std::fopen(inputName.c_str(), "rt");
            if (f) {
                char buf[1000+1];
                
                while(std::fgets(buf, 1000, f)) {
                    int len = (int)strlen(buf);
                    
                    while(len > 0 && isspace(buf[len-1]))
                        len--;
                    
                    buf[len] = '\0';
                    std::cout << "file " << buf << "\n";
                    image = cv::imread( buf, 1 );
                    
                    if (!image.empty()) {
                        detectAndDraw(image, cascade, nestedCascade, scale, tryflip);
                        char c {static_cast<char>(cv::waitKey(0))};
                        if ( c == 27 || c == 'q' || c == 'Q' )
                            break;
                    } else {
                        std::cerr << "Aw snap, couldn't read image " << buf << "\n";
                    }
                }
                std::fclose(f);
            }
        }
    }

    return 0;
}

void detectAndDraw(cv::Mat& img, cv::CascadeClassifier& cascade,
                   cv::CascadeClassifier& nestedCascade,
                   double scale, bool tryflip)
{
    double t {0};
    std::vector<cv::Rect> faces {}, faces2 {};
    
    const static cv::Scalar colors[] =
    {
        cv::Scalar(255,0,0),
        cv::Scalar(255,128,0),
        cv::Scalar(255,255,0),
        cv::Scalar(0,255,0),
        cv::Scalar(0,128,255),
        cv::Scalar(0,255,255),
        cv::Scalar(0,0,255),
        cv::Scalar(255,0,255)
    };
    
    cv::Mat gray {}, smallImg {};

    cv::cvtColor(img, gray, cv::COLOR_BGR2GRAY);
    double fx {1 / scale};
    cv::resize(gray, smallImg, cv::Size(), fx, fx, cv::INTER_LINEAR_EXACT);
    cv::equalizeHist(smallImg, smallImg);

    t = static_cast<double>(cv::getTickCount());
    cascade.detectMultiScale(smallImg, faces,
                             1.1, 2, 0
                             //|CASCADE_FIND_BIGGEST_OBJECT
                             //|CASCADE_DO_ROUGH_SEARCH
                             |cv::CASCADE_SCALE_IMAGE,
                             cv::Size(30, 30) );
    if (tryflip)
    {
        cv::flip(smallImg, smallImg, 1);
        cascade.detectMultiScale(smallImg, faces2,
                                 1.1, 2, 0
                                 //|CASCADE_FIND_BIGGEST_OBJECT
                                 //|CASCADE_DO_ROUGH_SEARCH
                                 |cv::CASCADE_SCALE_IMAGE,
                                 cv::Size(30, 30));
        for (std::vector<cv::Rect>::const_iterator r = faces2.begin(); r != faces2.end(); ++r)
        {
            faces.push_back(cv::Rect(smallImg.cols - r->x - r->width, r->y, r->width, r->height));
        }
    }
    
    t = static_cast<double>(cv::getTickCount() - t);
    printf("detection time = %g ms\n", t*1000/cv::getTickFrequency());
    
    for (size_t i = 0; i < faces.size(); i++)
    {
        cv::Rect r = faces[i];
        cv::Mat smallImgROI                 {};
        std::vector<cv::Rect> nestedObjects {};
        cv::Point center                    {};
        cv::Scalar color                    {colors[i%8]};
        int radius                          {};

        double aspect_ratio {static_cast<double>(r.width/r.height)};
        if (0.75 < aspect_ratio && aspect_ratio < 1.3)
        {
            center.x = cvRound((r.x + r.width*0.5)*scale);
            center.y = cvRound((r.y + r.height*0.5)*scale);
            radius   = cvRound((r.width + r.height)*0.25*scale);
            cv::circle(img, center, radius, color, 3, 8, 0);
        }
        else {
            cv::rectangle(img, cv::Point(cvRound(r.x*scale), cvRound(r.y*scale)),
                          cv::Point(cvRound((r.x + r.width-1)*scale),
                                    cvRound((r.y + r.height-1)*scale)),
                          color, 3, 8, 0);
        }
        
        if (nestedCascade.empty())
            continue;

        smallImgROI = smallImg(r);
        nestedCascade.detectMultiScale(smallImgROI, nestedObjects,
                                       1.1, 2, 0
                                       //|CASCADE_FIND_BIGGEST_OBJECT
                                       //|CASCADE_DO_ROUGH_SEARCH
                                       //|CASCADE_DO_CANNY_PRUNING
                                       |cv::CASCADE_SCALE_IMAGE,
                                       cv::Size(30, 30) );
        for (size_t j = 0; j < nestedObjects.size(); j++) {
            cv::Rect nr {nestedObjects[j]};
            center.x = cvRound((r.x + nr.x + nr.width*0.5)*scale);
            center.y = cvRound((r.y + nr.y + nr.height*0.5)*scale);
            radius   = cvRound((nr.width + nr.height)*0.25*scale);
            cv::circle(img, center, radius, color, 3, 8, 0);
        }
    }
    cv::imshow( "result", img );
}