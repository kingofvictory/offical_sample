#include <iostream>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/core/utility.hpp>

cv::Mat img   {};
int threshval {100};

static void on_trackbar(int, void*)
{
    cv::Mat bw         {threshval < 128 ? (img < threshval) : (img > threshval)};
    cv::Mat labelImage {img.size(), CV_32S};
    int Labels         {connectedComponents(bw, labelImage, 8)};

    std::vector<cv::Vec3b> colors(Labels);
    colors[0] = cv::Vec3b(0, 0, 0); // background
    
    for (int label = 1; label < Labels; ++label) {
        colors[label] = cv::Vec3b((rand()&255), (rand()&255), (rand()&255));
    }
    cv::Mat dst {img.size(), CV_8UC3};
    
    for (int r = 0; r < dst.rows; ++r) {
        for (int c = 0; c < dst.cols; ++c) {
            int label        {labelImage.at<int>(r, c)};
            cv::Vec3b &pixel {dst.at<cv::Vec3b>(r, c)};
            pixel            = colors[label];
         }
     }

    cv::imshow("Connected Components", dst);
}

int main( int argc, const char** argv )
{
    cv::CommandLineParser parser(argc, argv,
                                 "{@image|stuff.jpg|image for converting to a grayscale}");
    
    parser.about("\nThis program demonstrates connected components and use of the trackbar\n");
    parser.printMessage();
    std::cout << "\nThe image is converted to grayscale and displayed, another image has a trackbar\n"
        "that controls thresholding and thereby the extracted contours which are drawn in color\n";

    cv::String inputImage = parser.get<std::string>(0);
    img = cv::imread(cv::samples::findFile(inputImage), cv::IMREAD_GRAYSCALE);

    if (img.empty()) {
        std::cout << "Could not read input image file: " << inputImage << "\n";
        return EXIT_FAILURE;
    }

    cv::imshow("Image", img);
    cv::namedWindow("Connected Components", cv::WINDOW_AUTOSIZE);
    cv::createTrackbar("Threshold", "Connected Components", &threshval, 255, on_trackbar);
    on_trackbar(threshval, 0);

    cv::waitKey(0);
    return EXIT_SUCCESS;
}